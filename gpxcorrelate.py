import gpxpy
from gpxpy.gpx import GPXTrackPoint
from bisect import bisect
from datetime import datetime, timezone, timedelta
import click
import os
import subprocess
import json
import threading
import multiprocessing
from progress.bar import Bar

# point has the following members:
# latitude (type: float), longitude (type: float), elevation (type: float), time (type: datetime.datetime), speed, ...


def createHtml(points):
    """ based on https://wiki.openstreetmap.org/wiki/OpenLayers_Marker_Example """

    markers = ""
    setCenter = True
    for i, (img, point) in enumerate(points):
        if point is None:
            continue

        markers += """
    var lonLat{nb} = addMarker({lon}, {lat}, "{img}", "{basename}")
""".format(nb=i, lon=point.longitude, lat=point.latitude, img=img, basename=os.path.basename(img))
        if setCenter:
            setCenter = False
            markers += """
    var center = lonLat{nb}
""".format(nb=i)

    addMarkerFct = """
    function addMarker(lon, lat, img, basename)
    {
        var lonLat = new OpenLayers.LonLat(lon, lat)
              .transform(
                new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                map.getProjectionObject() // to Spherical Mercator Projection
              );

        // inspired by http://dev.openlayers.org/examples/popupMatrix.html
        var feature = new OpenLayers.Feature(markers, lonLat);
        feature.closeBox = false;
        feature.popupClass = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {'autoSize': true});
        feature.data.popupContentHTML = '<img src="'.concat(img, '" alt="" style="max-width:450px; max-height:450px;"></img><div>',  basename, '</div>');
        feature.data.overflow = "auto";

        var marker = feature.createMarker();

        var markerClick = function (evt) {
            if (this.popup == null) {
                this.popup = this.createPopup(this.closeBox);
                map.addPopup(this.popup);
                this.popup.show();
            } else {
                this.popup.toggle();
            }
            currentPopup = this.popup;
            OpenLayers.Event.stop(evt);
        };
        marker.events.register("mouseover", feature, markerClick);
        marker.events.register("mouseout", feature, markerClick);

        markers.addMarker(marker);
        return lonLat
    }
"""
    content = """
<html><body>
  <div id="mapdiv"></div>
  <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
  <script>
    {}

    map = new OpenLayers.Map("mapdiv");
    map.addLayer(new OpenLayers.Layer.OSM());

    var markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);

    {}

    var zoom=16;
    map.setCenter (center, zoom);
  </script>
</body></html>
""".format(addMarkerFct, markers)

    with open("markers.html", "w") as f:
        f.write(content)

def getGpxFiles(path):
    if not os.path.isdir(path):
        return [path]

    result = []
    for f in os.listdir(path):
        f = os.path.join(path, f)
        if os.path.isfile(f) and f.lower().endswith(".gpx"):
            result.append(os.path.realpath(f))

    return result

def getGpsPoints(paths):
    gps = None
    points = []
    for path in paths:
        for filename in getGpxFiles(path):
            with open(filename, 'r') as f:
                gps = gpxpy.parse(f)

            if gps is None:
                continue

            for track in gps.tracks:
                for seg in track.segments:
                    for point in seg.points:
                        points.append(point)

    return sorted(points, key=lambda p: p.time)

def interpolLin(v1, v2, f):
    if v1 is None or v2 is None:
        return None

    return v1 + (v2-v1)*f

def correlate(time, points, keys, timeGapInSeconds):
    i = bisect(keys, time)
    if i==0:
        return None # todo throw exception
    elif i==len(keys):
        if keys[i-1]==time:
            return points[i]
        else:
            return None # todo throw exception

    p1 = points[i-1]
    p2 = points[i]

    if p1.time==time:
        return p1

    time_diff1 = abs((time-p1.time).total_seconds())
    time_diff2 = abs((time-p2.time).total_seconds())
    if time_diff1>timeGapInSeconds or time_diff2>timeGapInSeconds:
        return None

    lin_adj = (time-p1.time)/(p2.time-p1.time)
    point = p1
    point.latitude = interpolLin(p1.latitude, p2.latitude, lin_adj)
    point.longitude = interpolLin(p1.longitude, p2.longitude, lin_adj)
    point.elevation = interpolLin(p1.elevation, p2.elevation, lin_adj)
    point.speed = interpolLin(p1.speed, p2.speed, lin_adj)
    point.time = time

    return point

def getExifData(f):
    r = subprocess.run(["exiftool", "-json", f], stdout=subprocess.PIPE)
    return json.loads(r.stdout)

def getDateTaken(f):
    exif = getExifData(f)[0]
    if "DateTimeOriginal" not in exif:
        return None

    dt = datetime.strptime(exif['DateTimeOriginal'], '%Y:%m:%d %H:%M:%S')
    return datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=timezone.utc)

def getNewestAndOldestTimestampFromPhotos(files):
    minTime = None
    maxTime = None

    for f in files:
        t = getDateTaken(f)
        if not minTime or t < minTime:
            minTime = t
        if not maxTime or t > maxTime:
            maxTime = t

    return minTime, maxTime

def photoHasGps(f):
    exif = getExifData(f)[0]
    return 'GPSLatitude' in exif

def getFilesToCorrelate(inputs):
    result = []
    for f in inputs:
        if os.path.isdir(f):
            for e in os.listdir(f):
                e = os.path.join(f, e)
                if os.path.isfile(e):
                    result.append(os.path.realpath(e))
        else:
            result.append(os.path.realpath(f))

    return result

def correlateImage(f, delta, points, keys, timeGapInSeconds):
    dt = getDateTaken(f)
    if dt is None:
        return None

    dt += delta
    coords = correlate(dt, points, keys, timeGapInSeconds)
    return coords

class CorrelateImageThread(threading.Thread):
    def __init__(self, f, delta, points, keys, timeGapInSeconds):
        super().__init__()
        self.f = f
        self.delta = delta
        self.points = points
        self.keys = keys
        self.timeGapInSeconds = timeGapInSeconds
        self.correlatedCoords = None

    def run(self):
        self.correlatedCoords = correlateImage(self.f, self.delta, self.points, self.keys, self.timeGapInSeconds)

def waitForCorrelateThreads(threads):
    correlations = []
    for t in threads:
        t.join()
        correlations.append((t.f, t.correlatedCoords))
    return correlations

def correlateImages(files, points, keys, offset="", timeGapInSeconds=10):
    correlations = []

    offset = [] if len(offset) == 0 else offset.split(":")
    h = 0
    m = 0
    s = 0

    if len(offset) > 3:
        # todo throw error
        pass
    elif len(offset) == 3:
        h = int(offset[0])
        m = int(offset[1])
        s = int(offset[2])
    elif len(offset) == 2:
        m = int(offset[0])
        s = int(offset[1])
    elif len(offset) == 1:
        s = int(offset[0])


    delta = timedelta(hours=h, minutes=m, seconds=s)
    maxThreads = multiprocessing.cpu_count()*2
    activeThreads = []

    for f in Bar('Correlating images').iter(files):
        if len(activeThreads) == maxThreads:
            correlations += waitForCorrelateThreads(activeThreads)
            activeThreads = []

        activeThreads.append(CorrelateImageThread(f, delta, points, keys, timeGapInSeconds))
        activeThreads[-1].start()

    correlations += waitForCorrelateThreads(activeThreads)
    return correlations

def listPhotosWithoutGeoInfo(files):
        noGps = []
        for f in Bar('Get photos without geo information').iter(files):
            if not photoHasGps(f):
                noGps.append(f)
        print("Photos without gps coordinates: {}".format(" ".join(noGps)))
def writeGeoInformationOfImage(f, gpsPoint):
    if not gpsPoint:
        return

    cmd = ["exiftool"]

    # for more information about the tags see https://exiftool.org/TagNames/GPS.html
    if gpsPoint.latitude:
        cmd.append("-GPSLatitude={}".format(abs(gpsPoint.latitude)))
        cmd.append("-GPSLatitudeRef={}".format("South" if gpsPoint.latitude<0 else "North"))
    if gpsPoint.longitude:
        cmd.append("-GPSLongitude={}".format(abs(gpsPoint.longitude)))
        cmd.append("-GPSLongitudeRef={}".format("West" if gpsPoint.longitude<0 else "East"))
    if gpsPoint.elevation:
        cmd.append("-GPSAltitude={}".format(abs(gpsPoint.elevation)))
        cmd.append("-GPSAltitudeRef={}".format("Below Sea Level" if gpsPoint.elevation<0 else "Above Sea Level"))
    if gpsPoint.speed:
        cmd.append("-GPSSpeed={}".format(gpsPoint.speed))
        #cmd.append("-GPSSpeedRef=K")

    cmd.append(f)
    subprocess.run(cmd, stdout=subprocess.PIPE)

def waitForThreads(threads):
    for t in threads:
        t.join()

def writeGeoInformation(corPoints):
    maxThreads = multiprocessing.cpu_count()*2
    activeThreads = []
    for f, point in Bar('Writing GPS information').iter(corPoints):
        if len(activeThreads) == maxThreads:
            waitForThreads(activeThreads)
            activeThreads = []

        activeThreads.append(threading.Thread(target=writeGeoInformationOfImage, args=(f, point)))
        activeThreads[-1].start()

    waitForThreads(activeThreads)

def performDryRun(corPoints):
    for f, point in corPoints:
        info = None
        if point:
            info = "lat: {}, lon: {}, altitude: {}, speed: {}".format(point.latitude, point.longitude, point.elevation, point.speed)
        print("{} -> {}".format(f, info))

def printSummary(corPoints):
    corCount = 0
    files = []
    for f, p in corPoints:
        if p:
            corCount += 1
        else:
            files.append(f)

    failedPictures = " Failed to correlate: {}".format(", ".join(files)) if len(files)>0 else ""
    print("Correlated {} images of {}.{}".format(corCount, len(corPoints), failedPictures))

def parseCoordinates(coordinates):
    ele = None
    if '@' in coordinates:
        latLon, ele = coordinates.split('@')
    else:
        latLon = coordinates

    lat, lon = latLon.split(',')

    if ele:
        ele = float(ele)

    if not (lat or lon):
        return None

    lat = float(lat)
    lon = float(lon)

    return GPXTrackPoint(latitude=lat, longitude=lon, elevation=ele)

@click.command()
@click.option('--gpx', '-g', multiple=True, type=click.Path(exists=True), help="Path to gpx-file used for correlation. You can also specify a directory containing gpx files. This option can be specified multiple times.")
@click.option('--showOnHtmlMap', '-s', is_flag=True, default=False, help="Create an html file that shows the correlated locations on a map.")
@click.option('--dryRun', is_flag=True, default=False, help="Show what would be done.")
@click.option('--update', '-u', is_flag=True, default=False, help="Update provided image files with correlated gps locations. Inactive in case of dryRun or showOnHtmlMap.")
@click.option('--offset', default="", help="Format: [-]ss, [-]mm:ss, [-]hh:mm:ss")
@click.option('--gap', default=10, help="Maximum time gap (in minutes) between an image and gps-points. Default is 10 minutes.")
@click.option('--coordinates', default="", help="Set gps-coordinates of images to provided value. Format: latitude,longitude[@elevation]. latitude/longitude in degrees. elevation in meters. E.g.: 47.598287,10.683562  or 47.598287,10.683562@654")
@click.option('--photosWithoutGeo', is_flag=True, default=False, help="List photos that do not have geo information")
@click.option('--compareTimestamps', is_flag=True, default=False, help="Compare timestamps of provided photos and gpx-files")
@click.argument('inputs', nargs=-1, type=click.Path(exists=True))
def main(gpx, showonhtmlmap, dryrun, update, offset, gap, coordinates, photoswithoutgeo, comparetimestamps, inputs):
    files = getFilesToCorrelate(inputs)
    points = getGpsPoints(list(gpx))
    keys = [x.time for x in points]

    if photoswithoutgeo:
        listPhotosWithoutGeoInfo(files)
        return

    if comparetimestamps:
        photoMinTime, photoMaxTime = getNewestAndOldestTimestampFromPhotos(files)
        gpxMinTime, gpxMaxTime = points[0].time, points[-1].time
        print(f"photo timestamps: {photoMinTime} - {photoMaxTime}")
        print(f"gpx timestamps: {gpxMinTime} - {gpxMaxTime}")
        return

    if coordinates:
        p = parseCoordinates(coordinates)
        corPoints = [(f, p) for f in files]
    else:
        corPoints = correlateImages(files, points, keys, offset, int(gap)*60)

    if showonhtmlmap or dryrun:
        update = False

    if showonhtmlmap:
        createHtml(corPoints)

    if dryrun:
        performDryRun(corPoints)

    if update and not (showonhtmlmap or dryrun):
        writeGeoInformation(corPoints)

    printSummary(corPoints)

if __name__ == '__main__':
    main()
